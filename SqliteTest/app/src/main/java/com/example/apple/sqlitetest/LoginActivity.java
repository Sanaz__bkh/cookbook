package com.example.apple.sqlitetest;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.sqlitetest.Database.DbHelper;
import com.example.apple.sqlitetest.Model.InfoArray;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    EditText email_txt,password_txt;
    Button login;

    List<InfoArray> infoArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email_txt = findViewById(R.id.email_txt);
        password_txt = findViewById(R.id.password_txt);
        login = findViewById(R.id.logBtn);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DbHelper db = new DbHelper(getBaseContext());
                infoArrayList = db.ReadFields();

                if (Userexists(infoArrayList) ) {

                    Intent intent = new Intent(LoginActivity.this, ListRecipeActivity.class);

                    startActivity(intent);
                } else {


                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(LoginActivity.this);

                    dlgAlert.setMessage("wrong password or email");
                    dlgAlert.setTitle("Error Message...");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                }
            }

        });


    }

    public void letsRegister(View view) {
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        startActivity(intent);
    }

    public boolean Userexists( List<InfoArray> infoArrayList){
        int i = 0;

        do{
            if (infoArrayList.get(i).getEmail().equals(email_txt.getText().toString()) && infoArrayList.get(i).getPassword().equals(password_txt.getText().toString())) {
                return true;
            }

            i += 1;


        }while (i < infoArrayList.size());
        return false;

    }




}
