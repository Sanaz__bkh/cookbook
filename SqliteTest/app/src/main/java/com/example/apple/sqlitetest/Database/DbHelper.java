package com.example.apple.sqlitetest.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.apple.sqlitetest.Model.InfoArray;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {
    public DbHelper(Context context) {
        super(context, "users", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE `userLogin` (\n" +
                "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`username`\tTEXT NOT NULL,\n" +
                "\t`email`\tTEXT NOT NULL,\n" +
                "\t`password`\tTEXT NOT NULL\n" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean insert(String username, String email,String password){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("username",username);
        cv.put("email",email);
        cv.put("password",password);
        long ret = db.insert("userLogin","id",cv);
        return ret>0;

    }

    public boolean update(int id,String username, String email,String password){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("username",username);
        cv.put("email",email);
        cv.put("password",password);
        int ret = db.update("userLogin",cv,"id = ?",new String[]{id+""} );
        return ret>0;
    }

    public boolean delete (int id ){
        SQLiteDatabase db = getWritableDatabase();
        int ret = db.delete("userLogin","id = ?",new String[]{id+""});

        return ret>0;
    }

    public List<InfoArray> ReadFields(){
        List<InfoArray> infoArrayList = new ArrayList<>();
        SQLiteDatabase db =  getReadableDatabase();
        Cursor cu = db.rawQuery("SELECT * FROM userLogin",null);
        if(cu.moveToFirst()){
            do{
                int id = cu.getInt(cu.getColumnIndex("id"));
                String username = cu.getString(cu.getColumnIndex("username"));
               String email = cu.getString(cu.getColumnIndex("email"));
                String password = cu.getString(cu.getColumnIndex("password"));
                InfoArray infoArray = new InfoArray(username,email,password);
                infoArrayList.add(infoArray);

            }while(cu.moveToNext());

        }
        return infoArrayList;

    }
}
