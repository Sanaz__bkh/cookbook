package com.example.apple.sqlitetest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.apple.sqlitetest.Database.DbHelper;
import com.example.apple.sqlitetest.Model.InfoArray;

public class MainActivity extends AppCompatActivity {
    EditText username_txt, email_txt, password_txt;
    Button regBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username_txt = findViewById(R.id.username_txt);
        email_txt = findViewById(R.id.email_txt);
        password_txt = findViewById(R.id.password_txt);
        regBtn = findViewById(R.id.regBtn);

        regBtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                DbHelper db = new DbHelper(getBaseContext());

                Boolean result = db.insert(username_txt.getText().toString(), email_txt.getText().toString(), password_txt.getText().toString());
                if(result == true) {
                    Intent intent = new Intent(MainActivity.this, ListRecipeActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
    }
}
