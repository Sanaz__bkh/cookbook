package WebServiceClasses;

import DatabaseConnectors.PreparedStatementJDBC;
import Model.InfoModel;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.List;

@Path("/service")
public class ServiceClass {
//    @Path("/insert")
//    @GET
//    @Produces("text/plain")
//    public String insertData(){
//    PreparedStatementJDBC psj = new PreparedStatementJDBC();
//    psj.insert("Portugal","silvia mercule","+351 9107641");
//    return "insert done";
//}

    @Path("/selectAppetizers")
    @GET
    @Produces("application/json")
    public List<InfoModel> selectAppetizersMethod(){
        PreparedStatementJDBC psj = new PreparedStatementJDBC();
        return psj.selectAllAppetizers();


    }
    @Path("/selectDesserts")
    @GET
    @Produces("application/json")
    public List<InfoModel> selectDessertsMethod(){
        PreparedStatementJDBC psj = new PreparedStatementJDBC();
        return psj.selectAllDesserts();


    }
    @Path("/selectMainMeals")
    @GET
    @Produces("application/json")
    public List<InfoModel> selectMainMethod(){
        PreparedStatementJDBC psj = new PreparedStatementJDBC();
        return psj.selectAllMain();


    }
    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {


        String uploadedFileLocation = "//Image//"
                + fileDetail.getFileName();

        // save it
        writeToFile(uploadedInputStream, uploadedFileLocation);

        String output = "File uploaded to : " + uploadedFileLocation;
        System.err.println(output);
        return Response.status(200).entity(output).build();

    }

    // save uploaded file to new location
    private void writeToFile(InputStream uploadedInputStream,
                             String uploadedFileLocation) {

        try {
            OutputStream out = new FileOutputStream(new File(
                    uploadedFileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    private static final String FILE_PATH = "/Users/apple/Desktop/images/test.png";
    @GET
    @Path("/getImage")
    @Produces("image/png")
    public Response getFile(){
        File file = new File(FILE_PATH);
        System.out.println(file);

        Response.ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition","attachment;filename=image_from_server.png");
        return response.build();
    }


}
