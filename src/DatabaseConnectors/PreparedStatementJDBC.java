package DatabaseConnectors;

import Model.InfoModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 12/3/2018 AD.
 */
public class PreparedStatementJDBC {
    Connection cnn;
    PreparedStatement st;



    public PreparedStatementJDBC() {
        try {
            Class.forName("com.mysql.jdbc.Driver");


            cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/turkishcookbook","root", "123");
        } catch (Exception e) {
            System.out.printf("mysql connection error");
        }
    }

//    public void insert(String country,String fullName,String phoneNumber){
//        try {
//            st = cnn.prepareStatement("insert into info (country,fullname,phoneNumber) values (?,?,?)");
//            st.setString(1,country);
//            st.setString(2,fullName);
//            st.setString(3,phoneNumber);
//
//            st.executeUpdate();
//
//        } catch (Exception e) {
//            System.out.println("insert error!");
//        }
//    }

    public List<InfoModel> selectAllAppetizers() {
        try {
            st = cnn.prepareStatement("select (id),(name),(imageAddress) from appetizers ");
            ResultSet r = st.executeQuery();
            List<InfoModel> mList = new ArrayList<>();

            while (r.next()) {
                InfoModel infoModel = new InfoModel();

                infoModel.setId(r.getInt("id"));

                infoModel.setName(r.getString("name"));

                infoModel.setImageAddress(r.getString("imageAddress"));
                mList.add(infoModel);

            }
            return mList;


        } catch (Exception e) {
            System.out.println("error in select function");
            return null;

        }
    }

        public List<InfoModel> selectAllDesserts() {
            try {
                st = cnn.prepareStatement("select * from desserts ");
                ResultSet r = st.executeQuery();
                List<InfoModel> mList = new ArrayList<>();

                while (r.next()) {
                    InfoModel infoModel = new InfoModel();

                    infoModel.setId(r.getInt("id"));

                    infoModel.setName(r.getString("name"));

                    infoModel.setImageAddress(r.getString("image"));
                    mList.add(infoModel);

                }
                return mList;


            } catch (Exception e) {
                System.out.println("error in select function");
                return null;

            }
        }


            public List<InfoModel> selectAllMain() {
                try {
                    st = cnn.prepareStatement("select * from mainMeals ");
                    ResultSet r = st.executeQuery();
                    List<InfoModel> mList = new ArrayList<>();

                    while (r.next()) {
                        InfoModel infoModel = new InfoModel();

                        infoModel.setId(r.getInt("id"));

                        infoModel.setName(r.getString("name"));

                        infoModel.setImageAddress(r.getString("image"));
                        mList.add(infoModel);

                    }
                    return mList;


                } catch (Exception e) {
                    System.out.println("error in select function");
                    return null;

                }

            }


    }


