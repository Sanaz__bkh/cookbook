package com.example.apple.newproject;

import android.app.admin.DeviceAdminReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import com.example.apple.newproject.Model.LoginModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.os.Environment.getExternalStorageDirectory;

public class DBHelper extends SQLiteAssetHelper {

    //private String DB_PATH = "/data/data/com.example.apple.newproject/databases/login.db";
    private static final String DATABASE_NAME = "login.db";
    private static final int DATABASE_VERSION= 1;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, context.getExternalFilesDir(null).getAbsolutePath(), null, DATABASE_VERSION);
    }


    public void insert(LoginModel loginModel) {


        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put("username",loginModel.getUsername());
        cv.put("email",loginModel.getEmail());
        cv.put("password",loginModel.getPassword());
        db.insert("userLogin","id",cv);
        db.setTransactionSuccessful();
        db.endTransaction();

    }



}
