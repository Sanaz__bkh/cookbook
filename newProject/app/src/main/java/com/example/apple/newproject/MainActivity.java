package com.example.apple.newproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apple.newproject.Model.LoginModel;

public class MainActivity extends AppCompatActivity {
    EditText username,email,password;
    Button registration;
    DBHelper dbHelper;
    LoginModel loginModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        registration = findViewById(R.id.registeration);
        registration.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                loginModel = new LoginModel(username.getText().toString(),email.getText().toString(),password.getText().toString());
                dbHelper = new DBHelper(getBaseContext());
                dbHelper.insert(loginModel);
                }

            });
        }


}
